-- --------------------------------------------------------
-- Hôte:                         127.0.0.1
-- Version du serveur:           8.0.30 - MySQL Community Server - GPL
-- SE du serveur:                Win64
-- HeidiSQL Version:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Listage de la structure de la base pour bonnieandcar
DROP DATABASE IF EXISTS `bonnieandcar`;
CREATE DATABASE IF NOT EXISTS `bonnieandcar` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `bonnieandcar`;

-- Listage de la structure de table bonnieandcar. article
DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `id` int NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(7000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime DEFAULT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table bonnieandcar.article : ~6 rows (environ)
DELETE FROM `article`;
INSERT INTO `article` (`id`, `title`, `content`, `slug`, `created_at`, `updated_at`) VALUES
	(1, 'gvhzjlgz', '<div>salut je suis un article</div>', 'gvhzjlgz', '2024-02-13 19:47:38', NULL),
	(2, 'heshtrerrshtrsbn', '<div>rstnrtstbsjlbnhmsrojhbntmeqùr</div>', 'heshtrerrshtrsbn', '2024-02-13 19:47:44', NULL),
	(3, 'super article !', '<div>qermougyhrequophrefvuopmberyqpbghqerpbouheh(eç^pbqo</div>', 'super-article', '2024-02-13 19:47:52', NULL),
	(4, 'wouauh !!!', '<div>qejhbreibrehpqobirzuçàghbiuhuçrdtyndpiufnd</div>', 'wouauh', '2024-02-13 19:48:05', NULL),
	(5, 'bien vue l\'aveugle !', '<div>qeruoyhez^pire$q^ksoiprtut^p jiohfonbrsuliyhbpstrmoh</div>', 'bien-vue-laveugle', '2024-02-13 19:48:15', NULL),
	(6, 'Lorem Ipsum', '<h1>Lorem Ipsum</h1><div><em><br>"Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit..."<br></em><br></div><div><br>"Il ny a personne qui naime la souffrance pour elle-même, qui ne la recherche et qui ne la veuille pour elle-même..."<figure data-trix-attachment="{&quot;contentType&quot;:&quot;image&quot;,&quot;height&quot;:14,&quot;url&quot;:&quot;https://a.pub.network/core/imgs/fslogo-green.svg&quot;,&quot;width&quot;:14}" data-trix-content-type="image" class="attachment attachment--preview"><img src="https://a.pub.network/core/imgs/fslogo-green.svg" width="14" height="14"><figcaption class="attachment__caption"></figcaption></figure><figure data-trix-attachment="{&quot;contentType&quot;:&quot;image&quot;,&quot;height&quot;:14,&quot;url&quot;:&quot;https://a.pub.network/core/imgs/fslogo-green.svg&quot;,&quot;width&quot;:14}" data-trix-content-type="image" class="attachment attachment--preview"><img src="https://a.pub.network/core/imgs/fslogo-green.svg" width="14" height="14"><figcaption class="attachment__caption"></figcaption></figure><br><br></div><div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec laoreet, orci eu lobortis vulputate, enim orci volutpat tortor, sit amet pharetra libero erat sit amet massa. Sed lacinia nisl eu dolor tempus consectetur. Aenean bibendum vestibulum quam a dignissim. Nam a risus vel sapien fermentum interdum. Sed sit amet aliquet massa. Donec faucibus metus quis placerat consequat. Donec eget nibh felis. Sed feugiat augue eu fringilla ultrices. Donec mattis nisl ut urna bibendum eleifend. Praesent vel felis sed mauris ullamcorper congue. Quisque pellentesque at eros vitae commodo. Curabitur ac eleifend ante. Nulla dignissim ultrices luctus.<br><br></div><div>Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla convallis in ante in ultrices. Phasellus posuere eu libero at varius. Proin pretium efficitur lectus rutrum malesuada. Integer tempus, turpis non iaculis porttitor, tortor turpis dapibus turpis, nec vulputate libero augue ac quam. Proin at leo tortor. Sed hendrerit turpis vel vulputate ornare. Praesent placerat eget libero sed volutpat. Sed in dui sollicitudin, interdum arcu in, semper nisl. Nullam felis eros, egestas sed hendrerit et, blandit eu ipsum. Pellentesque blandit nisl vel convallis consequat. Nunc interdum congue dignissim.<br><br></div><div>Sed rutrum augue sed nisl molestie aliquet. Mauris vitae tortor eu justo consequat condimentum. Vestibulum molestie vitae dui sit amet porttitor. Donec dictum lacus vel mauris scelerisque, ac rutrum lorem tincidunt. Aenean condimentum dolor quam, vitae pharetra neque pretium non. Morbi fringilla, quam vel elementum scelerisque, lectus ligula maximus ante, eget laoreet nibh urna at tortor. Cras quis cursus sapien. Suspendisse bibendum, leo eget rutrum venenatis, ipsum quam cursus tortor, a vulputate dui ante at ligula. Praesent ligula ipsum, blandit a imperdiet a, ultrices et massa. Sed porta, purus eget sollicitudin dignissim, lectus quam semper ligula, nec euismod ante nisl vel erat. Nunc ac porta neque. Aenean eget nisl nisl. Fusce nisi lectus, molestie convallis placerat id, auctor id neque. Vestibulum malesuada porttitor sapien, id ornare risus consectetur ut.<br><br></div><div>Nunc commodo, lacus sit amet pulvinar fermentum, nunc arcu aliquam elit, nec blandit purus mauris quis felis. Mauris interdum mattis augue in tempus. Pellentesque sagittis, purus quis sodales vulputate, massa sem aliquam lacus, ac egestas purus tortor ut dui. Nullam tincidunt lacus vitae enim scelerisque, eu hendrerit elit elementum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Suspendisse potenti. Donec dolor urna, venenatis a turpis non, laoreet faucibus neque. Proin vitae nunc mauris. Quisque tempus erat dolor.<br><br></div><div>Morbi eleifend sodales nulla. Morbi maximus, urna ac interdum ullamcorper, ipsum sem malesuada neque, eget tempus sem risus eu arcu. Nunc congue dictum arcu eget ultricies. Etiam at risus laoreet tellus faucibus placerat nec ut odio. Donec vitae mi eu lorem suscipit vulputate vitae non tellus. Maecenas ac mauris posuere, blandit lectus in, iaculis lacus. Maecenas condimentum, orci vel blandit tincidunt, arcu nisi tempor purus, ac accumsan enim odio in justo. Vestibulum nec sollicitudin nulla, ac venenatis ante. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum convallis velit suscipit ex faucibus scelerisque. Aliquam egestas eget enim et commodo. Vivamus non tempor augue. Sed sit amet lorem sit amet neque cursus viverra at sed lacus.<br><br></div><div><br></div>', 'lorem-ipsum', '2024-02-16 02:22:55', NULL);

-- Listage de la structure de table bonnieandcar. favorite
DROP TABLE IF EXISTS `favorite`;
CREATE TABLE IF NOT EXISTS `favorite` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id_id` int DEFAULT NULL,
  `vehicle_id_id` int DEFAULT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_68C58ED99D86650F` (`user_id_id`),
  KEY `IDX_68C58ED91DEB1EBB` (`vehicle_id_id`),
  CONSTRAINT `FK_68C58ED91DEB1EBB` FOREIGN KEY (`vehicle_id_id`) REFERENCES `vehicle` (`id`),
  CONSTRAINT `FK_68C58ED99D86650F` FOREIGN KEY (`user_id_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table bonnieandcar.favorite : ~4 rows (environ)
DELETE FROM `favorite`;
INSERT INTO `favorite` (`id`, `user_id_id`, `vehicle_id_id`, `date_add`) VALUES
	(2, 1, 2, '2023-02-13 16:41:20'),
	(3, 1, 4, '2022-02-13 16:41:32'),
	(38, 1, 3, '2024-02-13 22:47:43'),
	(44, 1, 1, '2024-02-14 22:17:25');

-- Listage de la structure de table bonnieandcar. orders
DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id_id` int DEFAULT NULL,
  `vehicle_id_id` int DEFAULT NULL,
  `orders_date` datetime NOT NULL,
  `cgp_validation` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E52FFDEE9D86650F` (`user_id_id`),
  KEY `IDX_E52FFDEE1DEB1EBB` (`vehicle_id_id`),
  CONSTRAINT `FK_E52FFDEE1DEB1EBB` FOREIGN KEY (`vehicle_id_id`) REFERENCES `vehicle` (`id`),
  CONSTRAINT `FK_E52FFDEE9D86650F` FOREIGN KEY (`user_id_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table bonnieandcar.orders : ~0 rows (environ)
DELETE FROM `orders`;

-- Listage de la structure de table bonnieandcar. product_for_sale
DROP TABLE IF EXISTS `product_for_sale`;
CREATE TABLE IF NOT EXISTS `product_for_sale` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id_id` int DEFAULT NULL,
  `vehicle_id_id` int DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `publicated_date` datetime NOT NULL,
  `sale` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_FD4ECB079D86650F` (`user_id_id`),
  KEY `IDX_FD4ECB071DEB1EBB` (`vehicle_id_id`),
  CONSTRAINT `FK_FD4ECB071DEB1EBB` FOREIGN KEY (`vehicle_id_id`) REFERENCES `vehicle` (`id`),
  CONSTRAINT `FK_FD4ECB079D86650F` FOREIGN KEY (`user_id_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table bonnieandcar.product_for_sale : ~0 rows (environ)
DELETE FROM `product_for_sale`;

-- Listage de la structure de table bonnieandcar. user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified` tinyint(1) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` int DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_number` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table bonnieandcar.user : ~4 rows (environ)
DELETE FROM `user`;
INSERT INTO `user` (`id`, `email`, `roles`, `password`, `first_name`, `last_name`, `email_verified`, `address`, `postal_code`, `city`, `phone_number`) VALUES
	(1, 'toto@toto.fr', '["ROLE_ADMIN"]', '$2y$13$l/G0ZcB8r1f/2Cg0Uj9yuOKK8GijfLXbjM3ASNoIlbBXtP3GK.gt.', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(3, 'test@test.fr', '["ROLE_USER"]', '$2y$13$KURB84oRwVTAylgmUDtCgeLJkYuUJEWYBRIvcg1OCXpCoNjTkulne', 'mael', 'badet', NULL, '3 rue de potier', 33000, 'Bordeaux', NULL),
	(5, 'mael@mael.fr', '["ROLE_USER"]', '$2y$13$GNzaIX2BUtP0yRiFqoadyOAtNUSbS1blbyRGnZCOpmmth0UNe60DO', 'mael', 'badet', NULL, '15 chemin des carreaux', 16290, 'Saint-Saturnin', 651654941),
	(7, 'grzuieoyghiaq\'@vsqlivgrzez.com', '["ROLE_USER"]', '$2y$13$loq5wzHFy5zCegkuk.tL9uVDs.k045gOzgHqROmBAv1YOb5ftrW2i', NULL, NULL, 0, NULL, NULL, NULL, NULL);

-- Listage de la structure de table bonnieandcar. vehicle
DROP TABLE IF EXISTS `vehicle`;
CREATE TABLE IF NOT EXISTS `vehicle` (
  `id` int NOT NULL AUTO_INCREMENT,
  `vehicle_filters_id_id` int DEFAULT NULL,
  `production_year` datetime NOT NULL,
  `circulation_date` datetime NOT NULL,
  `technical_revision` tinyint(1) NOT NULL,
  `number_owners` int NOT NULL,
  `kilometers` int DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doors` int NOT NULL,
  `places` int NOT NULL,
  `lenght` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1B80E486879B2297` (`vehicle_filters_id_id`) USING BTREE,
  CONSTRAINT `FK_1B80E486415CB75E` FOREIGN KEY (`vehicle_filters_id_id`) REFERENCES `vehicle_filters` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table bonnieandcar.vehicle : ~10 rows (environ)
DELETE FROM `vehicle`;
INSERT INTO `vehicle` (`id`, `vehicle_filters_id_id`, `production_year`, `circulation_date`, `technical_revision`, `number_owners`, `kilometers`, `color`, `doors`, `places`, `lenght`) VALUES
	(1, 1, '2011-01-01 00:00:00', '2022-02-15 00:00:00', 1, 1, 115000, 'gris', 4, 5, 4.5),
	(2, 2, '2020-05-01 00:00:00', '2020-06-20 00:00:00', 1, 2, 35000, 'noir', 5, 5, 4.2),
	(3, 3, '2005-03-01 00:00:00', '2023-04-10 00:00:00', 1, 1, 140000, 'rouge', 4, 5, 4.8),
	(4, 4, '2019-08-01 00:00:00', '2019-09-10 00:00:00', 1, 1, 8000, 'Black', 2, 2, 2),
	(5, 5, '2021-11-01 00:00:00', '2022-01-20 00:00:00', 1, 1, 12000, 'Silver', 3, 2, 2.5),
	(6, 6, '2020-03-01 00:00:00', '2020-04-15 00:00:00', 1, 2, 18000, 'Green', 2, 2, 2.2),
	(7, 7, '2018-06-01 00:00:00', '2018-07-10 00:00:00', 1, 1, 6000, 'Yellow', 1, 1, 1.8),
	(8, 8, '2017-04-01 00:00:00', '2017-05-20 00:00:00', 1, 2, 22000, 'Orange', 3, 4, 3.5),
	(9, 9, '2021-09-01 00:00:00', '2021-10-10 00:00:00', 1, 1, 15000, 'Purple', 2, 2, 2.3),
	(10, 10, '2024-02-12 20:45:24', '2024-02-12 20:45:25', 1, 0, 30, 'blue', 5, 4, 5);

-- Listage de la structure de table bonnieandcar. vehicle_filters
DROP TABLE IF EXISTS `vehicle_filters`;
CREATE TABLE IF NOT EXISTS `vehicle_filters` (
  `id` int NOT NULL AUTO_INCREMENT,
  `constructor` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `buy_price` int DEFAULT NULL,
  `location_price` int DEFAULT NULL,
  `two_wheels` tinyint(1) DEFAULT NULL,
  `four_wheels` tinyint(1) DEFAULT NULL,
  `trim` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cnit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tvv` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fueltype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hybrid` tinyint(1) DEFAULT NULL,
  `fiscal_horse_power` int DEFAULT NULL,
  `max_power` int DEFAULT NULL,
  `transmission` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urban_consumption` double DEFAULT NULL,
  `non_urban_consumption` double DEFAULT NULL,
  `average_consumption` double NOT NULL,
  `co2_emission` double NOT NULL,
  `v9critary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `j1critary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cylinder` int DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `guarantee` tinyint(1) DEFAULT NULL,
  `guarantee_limit_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table bonnieandcar.vehicle_filters : ~10 rows (environ)
DELETE FROM `vehicle_filters`;
INSERT INTO `vehicle_filters` (`id`, `constructor`, `model`, `buy_price`, `location_price`, `two_wheels`, `four_wheels`, `trim`, `cnit`, `tvv`, `fueltype`, `hybrid`, `fiscal_horse_power`, `max_power`, `transmission`, `urban_consumption`, `non_urban_consumption`, `average_consumption`, `co2_emission`, `v9critary`, `j1critary`, `cylinder`, `description`, `guarantee`, `guarantee_limit_date`) VALUES
	(1, 'Peugeot', '308', 10980, 154, 0, 1, 'Sedan', 'CNIT123', 'TVV456', 'Diesel', 0, 120, 180, 'automatique', 8.5, 6, 7, 150, '6', '1', 4, 'SEAT Leon\nLeon iv 1.5 tsi 150 xcellence one', 1, '2024-02-12 16:32:34'),
	(2, 'Seat', 'Leon', 20250, 154, 0, 1, 'Hatchback', 'CNIT789', 'TVV012', 'Essence', 0, 110, 170, 'manuelle', 9, 5.5, 7.2, 140, '5', '2', 4, 'SEAT Leon Leon iv 1.5 tsi 150 xcellence one', 0, '2019-02-02 16:32:38'),
	(3, 'Porshe', 'Cayenne', 6800, 100, 0, 1, 'Sedan', 'CNIT456', 'TVV789', 'Essence', 0, 130, 450, 'automatique', 9, 6, 16, 140, '5', '1', 4, NULL, 1, '2024-02-12 16:32:04'),
	(4, 'Harley-Davidson', 'Sportster', 12000, 3000, 1, 0, 'Cruiser', 'CNIT789', 'TVV555', 'Petrol', 0, 60, 80, '', 5, 4, 4.5, 90, 'Euro 4', 'Classic', 2, NULL, NULL, NULL),
	(5, 'Yamaha', 'MT-07', 9000, 2500, 1, 0, 'Naked', 'CNIT321', 'TVV222', 'Petrol', 0, 75, 90, '', 4.5, 3.5, 4, 80, 'Euro 5', 'Modern', 2, NULL, NULL, NULL),
	(6, 'Ducati', 'Monster', 15000, 3500, 1, 0, 'Streetfighter', 'CNIT456', 'TVV333', 'Petrol', 0, 100, 120, '', 6, 5, 5.5, 100, 'Euro 6', 'Sport', 2, NULL, NULL, NULL),
	(7, 'Vespa', 'Primavera', 5000, 1500, 1, 0, 'Scooter', 'CNIT999', 'TVV777', 'Petrol', 0, 30, 35, '', 3, 2.5, 2.8, 60, 'Euro 3', 'Classic', 1, NULL, NULL, NULL),
	(8, 'Piaggio', 'Liberty', 3500, 1200, 1, 0, 'Scooter', 'CNIT888', 'TVV666', 'Petrol', 0, 25, 30, '', 2.5, 2, 2.3, 50, 'Euro 2', 'Modern', 1, NULL, NULL, NULL),
	(9, 'Honda', 'Forza', 6000, 1800, 1, 0, 'Scooter', 'CNIT777', 'TVV555', 'Petrol', 0, 35, 40, '', 3.5, 3, 3.2, 70, 'Euro 4', 'Sport', 1, NULL, NULL, NULL),
	(10, 'Toyota', 'Corolla', 20000, 250, 0, 1, 'Sedan', 'ABC123', 'TVV123', 'hybrid', 0, 120, 150, 'manuelle', 6.5, 4.2, 5.4, 120, '1', 'CritB', 4, NULL, 0, '2024-02-13 00:51:05');

-- Listage de la structure de table bonnieandcar. vehicle_pictures
DROP TABLE IF EXISTS `vehicle_pictures`;
CREATE TABLE IF NOT EXISTS `vehicle_pictures` (
  `id` int NOT NULL AUTO_INCREMENT,
  `vehicle_id_id` int DEFAULT NULL,
  `pictures` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_526E20541DEB1EBB` (`vehicle_id_id`),
  CONSTRAINT `FK_526E20541DEB1EBB` FOREIGN KEY (`vehicle_id_id`) REFERENCES `vehicle` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table bonnieandcar.vehicle_pictures : ~8 rows (environ)
DELETE FROM `vehicle_pictures`;
INSERT INTO `vehicle_pictures` (`id`, `vehicle_id_id`, `pictures`) VALUES
	(1, 1, 'voiture-peugeot.png'),
	(2, 3, 'voiture-porshe.png'),
	(3, 2, 'voiture-seat.png'),
	(4, 4, 'moto-Harley-Davidson.png'),
	(5, 5, 'moto-Yamaha.png'),
	(6, 6, 'moto-Ducati.png'),
	(7, 6, 'voiture-porshe.png'),
	(8, 10, 'voiture-Toyota.png');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
