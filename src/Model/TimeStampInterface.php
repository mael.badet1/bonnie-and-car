<?php 

namespace App\Model;

use DateTimeImmutable;
use Doctrine\DBAL\Types\DateTimeImmutableType;

interface TimeStampInterface
{
    

    public function getCreatedAt();

    public function setCreatedAt(\DateTimeImmutable $createdAt);

    public function getUpdatedAt();

    public function setUpdatedAt(\DateTimeImmutable $updatedAt);
}