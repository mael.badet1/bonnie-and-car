<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AgentBCType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('prenom', TextType::class, [
                'label' => 'Prénom *'
            ])
            ->add('nom', TextType::class, [
                'label' => 'Nom *'
            ])
            ->add('email', TextType::class, [
                'label' => 'Adresse mail *'
            ])
            ->add('telephone', TextType::class, [
                'label' => 'Téléphone',
                'required' => false,
            ])
            ->add('mission', ChoiceType::class, [
                'choices'  => [
                    'Voitures' => false,
                    'Deux-roues' => true,
                ],
                'expanded'  => true,
                'multiple'  => true,
                'label' => 'Mission souhaitées *'

            ])
            ->add('codep', TextType::class, [
                'label' => 'Code postal *'
            ])
            ->add('message', TextType::class, [
                'label' => 'Message',
                'attr' => [
                    'class' => 'form-control',
                    'style' => 'height: 150px;',
                ],
                'label' => 'Message *',
            ])
            ->add('cgp', CheckboxType::class, [
                'label' => 'J’accepte les CGP et la politique de confidentialité de Bonnie&Car *'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
