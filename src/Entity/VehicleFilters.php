<?php

namespace App\Entity;

use App\Repository\VehicleFiltersRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VehicleFiltersRepository::class)]
class VehicleFilters
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $constructor = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $model = null;

    #[ORM\Column(nullable: true)]
    private ?int $buyPrice = null;

    #[ORM\Column(nullable: true)]
    private ?int $locationPrice = null;

    #[ORM\Column(nullable: true)]
    private ?bool $twoWheels = null;

    #[ORM\Column(nullable: true)]
    private ?bool $fourWheels = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $trim = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $cnit = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $tvv = null;

    #[ORM\Column(length: 255)]
    private ?string $fueltype = null;

    #[ORM\Column(nullable: true)]
    private ?bool $hybrid = null;

    #[ORM\Column(nullable: true)]
    private ?int $fiscalHorsePower = null;

    #[ORM\Column(nullable: true)]
    private ?int $maxPower = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $transmission = null;

    #[ORM\Column(nullable: true)]
    private ?float $urbanConsumption = null;

    #[ORM\Column(nullable: true)]
    private ?float $nonUrbanConsumption = null;

    #[ORM\Column]
    private ?float $averageConsumption = null;

    #[ORM\Column]
    private ?float $co2Emission = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $v9critary = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $j1critary = null;

    #[ORM\Column(nullable: true)]
    private ?int $cylinder = null;

    #[ORM\OneToMany(targetEntity: Vehicle::class, mappedBy: 'vehicleFiltersId')]
    private Collection $vehicles;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(nullable: true)]
    private ?bool $guarantee = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $guaranteeLimitDate = null;

    public function __construct()
    {
        $this->vehicles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getConstructor(): ?string
    {
        return $this->constructor;
    }

    public function setConstructor(?string $constructor): static
    {
        $this->constructor = $constructor;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(?string $model): static
    {
        $this->model = $model;

        return $this;
    }

    public function getBuyPrice(): ?int
    {
        return $this->buyPrice;
    }

    public function setBuyPrice(?int $buyPrice): static
    {
        $this->buyPrice = $buyPrice;

        return $this;
    }

    public function getLocationPrice(): ?int
    {
        return $this->locationPrice;
    }

    public function setLocationPrice(?int $locationPrice): static
    {
        $this->locationPrice = $locationPrice;

        return $this;
    }

    public function isTwoWheels(): ?bool
    {
        return $this->twoWheels;
    }

    public function setTwoWheels(?bool $twoWheels): static
    {
        $this->twoWheels = $twoWheels;

        return $this;
    }

    public function isFourWheels(): ?bool
    {
        return $this->fourWheels;
    }

    public function setFourWheels(?bool $fourWheels): static
    {
        $this->fourWheels = $fourWheels;

        return $this;
    }

    public function getTrim(): ?string
    {
        return $this->trim;
    }

    public function setTrim(?string $trim): static
    {
        $this->trim = $trim;

        return $this;
    }

    public function getCnit(): ?string
    {
        return $this->cnit;
    }

    public function setCnit(?string $cnit): static
    {
        $this->cnit = $cnit;

        return $this;
    }

    public function getTvv(): ?string
    {
        return $this->tvv;
    }

    public function setTvv(?string $tvv): static
    {
        $this->tvv = $tvv;

        return $this;
    }

    public function getFueltype(): ?string
    {
        return $this->fueltype;
    }

    public function setFueltype(string $fueltype): static
    {
        $this->fueltype = $fueltype;

        return $this;
    }

    public function isHybrid(): ?bool
    {
        return $this->hybrid;
    }

    public function setHybrid(?bool $hybrid): static
    {
        $this->hybrid = $hybrid;

        return $this;
    }

    public function getFiscalHorsePower(): ?int
    {
        return $this->fiscalHorsePower;
    }

    public function setFiscalHorsePower(?int $fiscalHorsePower): static
    {
        $this->fiscalHorsePower = $fiscalHorsePower;

        return $this;
    }

    public function getMaxPower(): ?int
    {
        return $this->maxPower;
    }

    public function setMaxPower(?int $maxPower): static
    {
        $this->maxPower = $maxPower;

        return $this;
    }

    public function getTransmission(): ?string
    {
        return $this->transmission;
    }

    public function setTransmission(?string $transmission): static
    {
        $this->transmission = $transmission;

        return $this;
    }

    public function getUrbanConsumption(): ?float
    {
        return $this->urbanConsumption;
    }

    public function setUrbanConsumption(?float $urbanConsumption): static
    {
        $this->urbanConsumption = $urbanConsumption;

        return $this;
    }

    public function getNonUrbanConsumption(): ?float
    {
        return $this->nonUrbanConsumption;
    }

    public function setNonUrbanConsumption(?float $nonUrbanConsumption): static
    {
        $this->nonUrbanConsumption = $nonUrbanConsumption;

        return $this;
    }

    public function getAverageConsumption(): ?float
    {
        return $this->averageConsumption;
    }

    public function setAverageConsumption(float $averageConsumption): static
    {
        $this->averageConsumption = $averageConsumption;

        return $this;
    }

    public function getCo2Emission(): ?float
    {
        return $this->co2Emission;
    }

    public function setCo2Emission(float $co2Emission): static
    {
        $this->co2Emission = $co2Emission;

        return $this;
    }

    public function getV9critary(): ?string
    {
        return $this->v9critary;
    }

    public function setV9critary(?string $v9critary): static
    {
        $this->v9critary = $v9critary;

        return $this;
    }

    public function getJ1critary(): ?string
    {
        return $this->j1critary;
    }

    public function setJ1critary(?string $j1critary): static
    {
        $this->j1critary = $j1critary;

        return $this;
    }

    public function getCylinder(): ?int
    {
        return $this->cylinder;
    }

    public function setCylinder(?int $cylinder): static
    {
        $this->cylinder = $cylinder;

        return $this;
    }

    /**
     * @return Collection<int, Vehicle>
     */
    public function getVehicles(): Collection
    {
        return $this->vehicles;
    }

    public function addVehicle(Vehicle $vehicle): static
    {
        if (!$this->vehicles->contains($vehicle)) {
            $this->vehicles->add($vehicle);
            $vehicle->setvehicleFiltersId($this);
        }

        return $this;
    }

    public function removeVehicle(Vehicle $vehicle): static
    {
        if ($this->vehicles->removeElement($vehicle)) {
            // set the owning side to null (unless already changed)
            if ($vehicle->getvehicleFiltersId() === $this) {
                $vehicle->setvehicleFiltersId(null);
            }
        }

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function isGuarantee(): ?bool
    {
        return $this->guarantee;
    }

    public function setGuarantee(?bool $guarantee): static
    {
        $this->guarantee = $guarantee;

        return $this;
    }

    public function getGuaranteeLimitDate(): ?\DateTimeInterface
    {
        return $this->guaranteeLimitDate;
    }

    public function setGuaranteeLimitDate(?\DateTimeInterface $guaranteeLimitDate): static
    {
        $this->guaranteeLimitDate = $guaranteeLimitDate;

        return $this;
    }
}
