<?php

namespace App\Entity;

use App\Repository\ProductForSaleRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProductForSaleRepository::class)]
class ProductForSale
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'productForSales')]
    private ?User $userId = null;

    #[ORM\ManyToOne(inversedBy: 'productForSales')]
    private ?vehicle $vehicleId = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $publicatedDate = null;

    #[ORM\Column(nullable: true)]
    private ?bool $sale = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?User
    {
        return $this->userId;
    }

    public function setUserId(?User $userId): static
    {
        $this->userId = $userId;

        return $this;
    }

    public function getVehicleId(): ?vehicle
    {
        return $this->vehicleId;
    }

    public function setVehicleId(?vehicle $vehicleId): static
    {
        $this->vehicleId = $vehicleId;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getPublicatedDate(): ?\DateTimeInterface
    {
        return $this->publicatedDate;
    }

    public function setPublicatedDate(\DateTimeInterface $publicatedDate): static
    {
        $this->publicatedDate = $publicatedDate;

        return $this;
    }

    public function isSale(): ?bool
    {
        return $this->sale;
    }

    public function setSale(?bool $sale): static
    {
        $this->sale = $sale;

        return $this;
    }
}
