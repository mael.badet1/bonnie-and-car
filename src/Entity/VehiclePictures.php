<?php

namespace App\Entity;

use App\Repository\VehiclePicturesRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VehiclePicturesRepository::class)]
class VehiclePictures
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'vehiclePictures')]
    private ?Vehicle $vehicleId = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $pictures = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVehicleId(): ?Vehicle
    {
        return $this->vehicleId;
    }

    public function setVehicleId(?Vehicle $vehicleId): static
    {
        $this->vehicleId = $vehicleId;

        return $this;
    }

    public function getPictures(): ?string
    {
        return $this->pictures;
    }

    public function setPictures(?string $pictures): static
    {
        $this->pictures = $pictures;

        return $this;
    }
}
