<?php

namespace App\Entity;

use App\Repository\VehicleRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VehicleRepository::class)]
class Vehicle
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'vehicles')]
    private ?VehicleFilters $vehicleFiltersId = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $productionYear = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $circulationDate = null;

    #[ORM\Column]
    private ?bool $technicalRevision = null;

    #[ORM\Column]
    private ?int $numberOwners = null;

    #[ORM\Column(nullable: true)]
    private ?int $kilometers = null;

    #[ORM\Column(length: 255)]
    private ?string $color = null;

    #[ORM\Column]
    private ?int $doors = null;

    #[ORM\Column]
    private ?int $places = null;

    #[ORM\Column]
    private ?float $lenght = null;

    #[ORM\OneToMany(targetEntity: VehiclePictures::class, mappedBy: 'vehicleId')]
    private Collection $vehiclePictures;

    #[ORM\OneToMany(targetEntity: Favorite::class, mappedBy: 'vehicleId')]
    private Collection $favorites;

    #[ORM\OneToMany(targetEntity: Orders::class, mappedBy: 'vehicleId')]
    private Collection $orders;

    #[ORM\OneToMany(targetEntity: ProductForSale::class, mappedBy: 'vehicleId')]
    private Collection $productForSales;

    public function __construct()
    {
        $this->vehiclePictures = new ArrayCollection();
        $this->favorites = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->productForSales = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getvehicleFiltersId(): ?VehicleFilters
    {
        return $this->vehicleFiltersId;
    }

    public function setvehicleFiltersId(?VehicleFilters $vehicleFiltersId): static
    {
        $this->vehicleFiltersId = $vehicleFiltersId;

        return $this;
    }

    public function getProductionYear(): ?\DateTimeInterface
    {
        return $this->productionYear;
    }

    public function setProductionYear(\DateTimeInterface $productionYear): static
    {
        $this->productionYear = $productionYear;

        return $this;
    }

    public function getCirculationDate(): ?\DateTimeInterface
    {
        return $this->circulationDate;
    }

    public function setCirculationDate(\DateTimeInterface $circulationDate): static
    {
        $this->circulationDate = $circulationDate;

        return $this;
    }

    public function isTechnicalRevision(): ?bool
    {
        return $this->technicalRevision;
    }

    public function setTechnicalRevision(bool $technicalRevision): static
    {
        $this->technicalRevision = $technicalRevision;

        return $this;
    }

    public function getNumberOwners(): ?int
    {
        return $this->numberOwners;
    }

    public function setNumberOwners(int $numberOwners): static
    {
        $this->numberOwners = $numberOwners;

        return $this;
    }

    public function getKilometers(): ?int
    {
        return $this->kilometers;
    }

    public function setKilometers(?int $kilometers): static
    {
        $this->kilometers = $kilometers;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): static
    {
        $this->color = $color;

        return $this;
    }

    public function getDoors(): ?int
    {
        return $this->doors;
    }

    public function setDoors(int $doors): static
    {
        $this->doors = $doors;

        return $this;
    }

    public function getPlaces(): ?int
    {
        return $this->places;
    }

    public function setPlaces(int $places): static
    {
        $this->places = $places;

        return $this;
    }

    public function getLenght(): ?float
    {
        return $this->lenght;
    }

    public function setLenght(float $lenght): static
    {
        $this->lenght = $lenght;

        return $this;
    }

    /**
     * @return Collection<int, VehiclePictures>
     */
    public function getVehiclePictures(): Collection
    {
        return $this->vehiclePictures;
    }

    public function addVehiclePicture(VehiclePictures $vehiclePicture): static
    {
        if (!$this->vehiclePictures->contains($vehiclePicture)) {
            $this->vehiclePictures->add($vehiclePicture);
            $vehiclePicture->setVehicleId($this);
        }

        return $this;
    }

    public function removeVehiclePicture(VehiclePictures $vehiclePicture): static
    {
        if ($this->vehiclePictures->removeElement($vehiclePicture)) {
            // set the owning side to null (unless already changed)
            if ($vehiclePicture->getVehicleId() === $this) {
                $vehiclePicture->setVehicleId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Favorite>
     */
    public function getFavorites(): Collection
    {
        return $this->favorites;
    }

    public function addFavorite(Favorite $favorite): static
    {
        if (!$this->favorites->contains($favorite)) {
            $this->favorites->add($favorite);
            $favorite->setVehicleId($this);
        }

        return $this;
    }

    public function removeFavorite(Favorite $favorite): static
    {
        if ($this->favorites->removeElement($favorite)) {
            // set the owning side to null (unless already changed)
            if ($favorite->getVehicleId() === $this) {
                $favorite->setVehicleId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Orders>
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Orders $order): static
    {
        if (!$this->orders->contains($order)) {
            $this->orders->add($order);
            $order->setVehicleId($this);
        }

        return $this;
    }

    public function removeOrder(Orders $order): static
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getVehicleId() === $this) {
                $order->setVehicleId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, ProductForSale>
     */
    public function getProductForSales(): Collection
    {
        return $this->productForSales;
    }

    public function addProductForSale(ProductForSale $productForSale): static
    {
        if (!$this->productForSales->contains($productForSale)) {
            $this->productForSales->add($productForSale);
            $productForSale->setVehicleId($this);
        }

        return $this;
    }

    public function removeProductForSale(ProductForSale $productForSale): static
    {
        if ($this->productForSales->removeElement($productForSale)) {
            // set the owning side to null (unless already changed)
            if ($productForSale->getVehicleId() === $this) {
                $productForSale->setVehicleId(null);
            }
        }

        return $this;
    }
}
