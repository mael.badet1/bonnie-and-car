<?php

namespace App\Entity;

use App\Repository\OrdersRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: OrdersRepository::class)]
class Orders
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\ManyToOne(inversedBy: 'orders')]
    private ?User $userId = null;

    #[ORM\ManyToOne(inversedBy: 'orders')]
    private ?Vehicle $vehicleId = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $ordersDate = null;

    #[ORM\Column(nullable: true)]
    private ?bool $cgpValidation = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?User
    {
        return $this->userId;
    }

    public function setUserId(?User $userId): static
    {
        $this->userId = $userId;

        return $this;
    }

    public function getVehicleId(): ?Vehicle
    {
        return $this->vehicleId;
    }

    public function setVehicleId(?Vehicle $vehicleId): static
    {
        $this->vehicleId = $vehicleId;

        return $this;
    }

    public function getOrdersDate(): ?\DateTimeInterface
    {
        return $this->ordersDate;
    }

    public function setOrdersDate(\DateTimeInterface $ordersDate): static
    {
        $this->ordersDate = $ordersDate;

        return $this;
    }

    public function isCgpValidation(): ?bool
    {
        return $this->cgpValidation;
    }

    public function setCgpValidation(?bool $cgpValidation): static
    {
        $this->cgpValidation = $cgpValidation;

        return $this;
    }
}
