<?php

namespace App\Repository;

use App\Entity\VehicleFilters;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<VehicleFilters>
 *
 * @method VehicleFilters|null find($id, $lockMode = null, $lockVersion = null)
 * @method VehicleFilters|null findOneBy(array $criteria, array $orderBy = null)
 * @method VehicleFilters[]    findAll()
 * @method VehicleFilters[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VehicleFiltersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VehicleFilters::class);
    }

//    /**
//     * @return VehicleFilters[] Returns an array of VehicleFilters objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('v.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?VehicleFilters
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
