<?php

namespace App\Repository;

use App\Entity\VehiclePictures;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<VehiclePictures>
 *
 * @method VehiclePictures|null find($id, $lockMode = null, $lockVersion = null)
 * @method VehiclePictures|null findOneBy(array $criteria, array $orderBy = null)
 * @method VehiclePictures[]    findAll()
 * @method VehiclePictures[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VehiclePicturesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, VehiclePictures::class);
    }

//    /**
//     * @return VehiclePictures[] Returns an array of VehiclePictures objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('v.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?VehiclePictures
//    {
//        return $this->createQueryBuilder('v')
//            ->andWhere('v.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
