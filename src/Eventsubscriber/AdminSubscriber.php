<?php

namespace App\Eventsubscriber;

use App\Model\TimeStampInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
use PHPUnit\Event\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AdminSubscriber implements EventSubscriberInterface 
{

    public static function getSubscribedEvents() :array
    {
        return [
            BeforeEntityPersistedEvent::class => ['setEntityCreatedAt'],
            BeforeEntityUpdatedEvent::class => ['setEntityUpdatedAt']
        ];
    }

    public function setEntityCreatedAt(BeforeEntityPersistedEvent $event) :void
    {
       $entity = $event->getEntityInstance();

       if (!$entity instanceof TimeStampInterface) {
            return;
       }
       else {
        $entity->setCreatedAt(new \DateTimeImmutable());
       }
    }

    public function setEntityUpdatedAt(BeforeEntityUpdatedEvent $event) :void
    {
    
       $entity = $event->getEntityInstance();


       if (!$entity instanceof TimeStampInterface) {
            return;
       }
       else {
        $entity->setUpdatedAt(new \DateTimeImmutable());
       }
    }
}