<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SessionController extends AbstractController
{
    #[Route('/visit', name: 'app_visit')]
    public function getVisit(Request $request): Response
    {
        $visitCount = $request->cookies->get('nbVisit', 0);
        $visitCount++;
        
        $response = new Response();
        $response->headers->setCookie(new Cookie('nbVisit', $visitCount));
        $response->send();

        return $this->render('session/index.html.twig', [
            'visit_count' => $visitCount,
            'controller_name' => 'VisitController',
        ]);
    }
}
