<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends AbstractController
{
    #[Route('/conseils', name: 'app_article', methods: ['GET'])]
    public function conseils(ArticleRepository $articleRepository): Response
    {
        return $this->render('conseils/conseils.html.twig', [
            'articles' => $articleRepository->findAll(),
        ]);
    }

    #[Route('/article/{id}', name: 'app_article_page')]
    public function pagearticle($id, EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Article::class);

       /* $id = intval($_GET['id']); */

        $larticle = $repository->find($id);

        return $this->render('article/index.html.twig', [
            'article' => $larticle,
        ]);
    }
}
