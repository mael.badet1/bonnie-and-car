<?php

namespace App\Controller;

use App\Entity\Favorite;
use App\Entity\VehicleFilters;
use App\Form\EditProfileDataType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PersonnalInformationController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/personnal/informations', name: 'app_personnal_information')]
    public function index(Request $request): Response
    {
        $user = $this->getUser();

        if (!$user) {
            return $this->redirectToRoute('app_login');
        }

        $form = $this->createForm(EditProfileDataType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();

            return $this->redirectToRoute('app_personnal_information');
        }

        return $this->render('personnal_information/personnalInformation.html.twig', [
            'controller_name' => 'PersonnalInformationController',
            'form' => $form->createView(),
        ]);
    }

    #[Route('/personnal/favories', name: 'app_personnal_favories')]
    public function favories(EntityManagerInterface $entityManager): Response
    {
        $user = $this->getUser();

        if (!$user) {
            return $this->redirectToRoute('app_login');
        }

        $repository = $entityManager->getRepository(Favorite::class);
        $userId = $user->getId();

        $fav = $repository->createQueryBuilder('f')
            ->select('DISTINCT f', 'v', 'vf')
            ->leftJoin('f.vehicleId', 'v')
            ->leftJoin('v.vehicleFiltersId', 'vf')
            ->where('f.userId = :userId')
            ->setParameter('userId', $userId)
            ->getQuery()
            ->getResult();

        $imagePaths = [];
        foreach ($fav as $favorites) {
            $imagePaths[] .= $this->buildImagePath($favorites);
        }

        return $this->render('personnal_information/favorite.html.twig', [
            'controller_name' => 'favories',
            'favoris' => $fav,
            'imagePaths' => $imagePaths,
        ]);
    }

    #[Route('/personnal/delete_favoris/{id}', name: 'app_delete_favoris', methods: ['GET'])]
    public function deleteFavoris(EntityManagerInterface $entityManager, $id): Response
    {
        $favoriRepository = $entityManager->getRepository(Favorite::class);
        $favori = $favoriRepository->find($id);

        if ($favori) {
            $entityManager->remove($favori);
            $entityManager->flush();
        }

        // Rediriger vers la page des favoris après la suppression
        return $this->redirectToRoute('app_personnal_favories');
    }

    private function buildImagePath(Favorite $fav): string
    {
        $vehicle = $fav->getVehicleId();
        $vehicleFilters = $vehicle->getvehicleFiltersId();
        if ($vehicleFilters instanceof VehicleFilters && $vehicleFilters->isFourWheels() === true) {
            return 'voiture-' . $vehicleFilters->getConstructor() . '.png';
        } elseif ($vehicleFilters instanceof VehicleFilters && $vehicleFilters->isTwoWheels() === true) {
            return 'moto-' . $vehicleFilters->getConstructor() . '.png';
        }
    }
}
