<?php

namespace App\Controller;

use App\Form\AgentBCType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DevenirAgentBCController extends AbstractController
{
    #[Route('/devenir_agent_bc', name: 'devenir_agent_form')]
    public function index(): Response
    {
        $form = $this->createForm(AgentBCType::class);

        if ($form->isSubmitted() && $form->isValid()) {

            return $this->redirectToRoute('devenir_agent_form');
        }

        return $this->render('devenir_agent_bc/index.html.twig', [
            'controller_name' => 'DevenirAgentBCController',
            'form' => $form->createView(),
        ]);
    }
}
