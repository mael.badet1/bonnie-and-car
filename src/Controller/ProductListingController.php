<?php

namespace App\Controller;

use App\Entity\Favorite;
use App\Entity\Vehicle;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductListingController extends AbstractController
{
    #[Route('/product/{id}', name: 'app_product_listing')]
    public function index($id, EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Vehicle::class);

        $vehicle = $repository->find($id);

        $vehicleFilters = $vehicle->getVehicleFiltersId();

        $vehiclePictures = $vehicle->getVehiclePictures();

        if (!$vehicle) {
            throw $this->createNotFoundException('Vehicle not found');
        } else {
            if ($vehicleFilters->istwowheels() === true) {
                $vehicleType = 'bikes';
            } else if ($vehicleFilters->isfourwheels() === true) {
                $vehicleType = 'cars';
            }
            return $this->render('product_listing/listing.html.twig', [
                'controller_name' => 'ProductListingController',
                'id' => $id,
                'vehicle' => $vehicle,
                'vehicleFilters' => $vehicleFilters,
                'vehiclePictures' => $vehiclePictures,
                'vehicleType' => $vehicleType,
            ]);
        }
    }

    #[Route('/personnal/favories', name: 'app_add_fav')]
    public function addFavCars(Request $request, EntityManagerInterface $entityManager): Response
    {
        $user = $this->getUser();
        if (!$user) {
            return $this->redirectToRoute('app_login');
        }

        $vehicleIdToAddToFavorites = $request->get('id');
        $isAlreadyFavorite = $entityManager->getRepository(Favorite::class)
            ->findOneBy([
                'userId' => $user,
                'vehicleId' => $vehicleIdToAddToFavorites,
            ]);

        if (!$isAlreadyFavorite) {
            $favorite = new Favorite();
            $favorite->setUserId($user);
            $vehicle = $entityManager->getRepository(Vehicle::class)->find($vehicleIdToAddToFavorites);
            $favorite->setVehicleId($vehicle);
            $favorite->setDateAdd(new \DateTime());
            $entityManager->persist($favorite);
            $entityManager->flush();
        } else {
            var_dump('hello');
            die;
            $entityManager->remove($isAlreadyFavorite);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_personnal_favories');
    }
}
