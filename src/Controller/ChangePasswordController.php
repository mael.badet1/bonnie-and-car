<?php

namespace App\Controller;

use App\Form\ChangePasswordType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class ChangePasswordController extends AbstractController
{
    private $userPasswordHasher;
    public function __construct(UserPasswordHasherInterface $userPasswordHasher)
    {
        $this->userPasswordHasher = $userPasswordHasher;
    }

    #[Route('/change-password', name: 'change_password')]
    public function changePassword(Request $request, UserPasswordHasherInterface $userPasswordHasher, EntityManagerInterface $entityManager): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(ChangePasswordType::class, $user);
        $form->handleRequest($request);
        $oldPass = $user->getPassword();
        $oldPassTape = $form->get('oldPassword')->getData();
        $newPassword = $form->get('newPassword')->getData();
        $verifNewpassword = $form->get('verifNewPassword')->getData();

        if ($form->isSubmitted() && $form->isValid()) {
            if ($oldPassTape !== $oldPass) {
                if ($oldPassTape !== $newPassword && $newPassword === $verifNewpassword) {
                    $user->setPassword(
                        $userPasswordHasher->hashPassword(
                            $user,
                            $form->get('newPassword')->getData()
                        )
                    );
                    $entityManager->persist($user);
                    $entityManager->flush();
                    return $this->redirectToRoute('app_home_page');
                } else {
                    $this->addFlash('danger', 'Mot de passe invalide. Êtes vous sur que vos mots passe correspondais ?');
                }
            } else {
                $this->addFlash('danger', 'Le mot de passe est le même que le précédent');
            }
        }
        $form = $this->createForm(ChangePasswordType::class);

        return $this->render('change_password/index.html.twig', [
            'changePasswordForm' => $form->createView(),
            'oldPassword' => $oldPass,
        ]);
    }
}
