<?php

namespace App\Controller;

use App\Entity\Favorite;
use App\Entity\Vehicle;
use App\Entity\VehicleFilters;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListingController extends AbstractController
{
    #[Route('/listing/cars', name: 'app_listing_cars')]
    public function carsListing(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Vehicle::class);

        $cars = $repository->createQueryBuilder('v')
            ->select('v', 'vf')
            ->leftJoin('v.vehicleFiltersId', 'vf')
            ->andWhere('vf.fourWheels = :fourWheels')
            ->setParameter('fourWheels', true)
            ->getQuery()
            ->getResult();

        $imagePathsCar = [];

        $filesystem = new Filesystem;

        foreach ($cars as $car) {
            $lechemin = $this->buildImagePath($car);
            if ($filesystem->exists('./images/cars/' . $lechemin)) {
                $imagePathsCar[] .= $this->buildImagePath($car);
            }
        }

        return $this->render('listing/cars.html.twig', [
            'cars' => $cars,
            'imagePathsCar' => $imagePathsCar,
        ]);
    }

    #[Route('/filtered/cars', name: 'app_filtered_cars')]
    public function filteredCars(Request $request, EntityManagerInterface $entityManager): Response
    {
        $fuelType = $request->request->get('fuelType') === '1' ? null : $request->request->get('fuelType');
        $constructor = $request->request->get('constructor') === '1' ? null : $request->request->get('constructor');
        $model = $request->request->get('model') === '1' ? null : $request->request->get('model');
        $transmission = $request->request->get('transmission') === '1' ? null : $request->request->get('transmission');
        $minPrice = empty($request->request->get('minPrice')) ? null : intval($request->request->get('minPrice'));
        $maxPrice = empty($request->request->get('maxPrice')) ? null : intval($request->request->get('maxPrice'));
        $minKilometer = empty($request->request->get('minKilometer')) ? null : intval($request->request->get('minKilometer'));
        $maxKilometer = empty($request->request->get('maxKilometer')) ? null : intval($request->request->get('maxKilometer'));
        $minYear = empty($request->request->get('minYear')) ? null : intval($request->request->get('minYear'));
        $maxYear = empty($request->request->get('maxYear')) ? null : intval($request->request->get('maxYear'));

        $repository = $entityManager->getRepository(Vehicle::class);

        $carsQueryBuilder = $repository->createQueryBuilder('v')
            ->select('v', 'vf')
            ->leftJoin('v.vehicleFiltersId', 'vf')
            ->andWhere('vf.fourWheels = :fourWheels')
            ->setParameter('fourWheels', true);

        if ($fuelType && $fuelType !== null) {
            $carsQueryBuilder->andWhere('vf.fueltype = :fuelType')
                ->setParameter('fuelType', $fuelType);
        }

        if ($constructor && $constructor !== null) {
            $carsQueryBuilder->andWhere('vf.constructor = :constructor')
                ->setParameter('constructor', $constructor);
        }

        if ($model && $model !== null) {
            $carsQueryBuilder->andWhere('vf.model = :model')
                ->setParameter('model', $model);
        }

        if ($transmission && $transmission !== null) {
            $carsQueryBuilder->andWhere('vf.transmission = :transmission')
                ->setParameter('transmission', $transmission);
        }

        if ($minPrice && $minPrice !== null) {
            $carsQueryBuilder->andWhere('vf.buyPrice >= :minPrice')
                ->setParameter('minPrice', $minPrice);
        }

        if ($maxPrice && $maxPrice !== null) {
            $carsQueryBuilder->andWhere('vf.buyPrice <= :maxPrice')
                ->setParameter('maxPrice', $maxPrice);
        }

        if ($minKilometer && $minKilometer !== null) {
            $carsQueryBuilder->andWhere('v.kilometers >= :minKilometer')
                ->setParameter('minKilometer', $minKilometer);
        }

        if ($maxKilometer && $maxKilometer !== null) {
            $carsQueryBuilder->andWhere('v.kilometers <= :maxKilometer')
                ->setParameter('maxKilometer', $maxKilometer);
        }

        if ($minYear && $minYear !== null) {
            $minYearDate = new \DateTime("$minYear-01-01 00:00:00");
            $carsQueryBuilder->andWhere('v.productionYear >= :minYear')
                ->setParameter('minYear', $minYearDate);
        }

        if ($maxYear && $maxYear !== null) {
            $maxYearDate = new \DateTime("$maxYear-12-31 23:59:59");
            $carsQueryBuilder->andWhere('v.productionYear <= :maxYear')
                ->setParameter('maxYear', $maxYearDate);
        }

        $filteredCars = $carsQueryBuilder->getQuery()->getResult();


        $imagePathsCar = [];

        $filesystem = new Filesystem;

        foreach ($filteredCars as $car) {
            $lechemin = $this->buildImagePath($car);
            if ($filesystem->exists('./images/cars/' . $lechemin)) {
                $imagePathsCar[] .= $this->buildImagePath($car);
            }
        }

        return $this->render('listing/cars.html.twig', [
            'cars' => $filteredCars,
            'imagePathsCar' => $imagePathsCar,
        ]);
    }

    #[Route('/listing/bikes', name: 'app_listing_bikes')]
    public function bikesListing(EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Vehicle::class);

        $bikes = $repository->createQueryBuilder('v')
            ->select('v', 'vf')
            ->leftJoin('v.vehicleFiltersId', 'vf')
            ->andWhere('vf.twoWheels = :twoWheels')
            ->setParameter('twoWheels', true)
            ->getQuery()
            ->getResult();

        $imagePathsBike = [];

        $filesystem = new Filesystem;

        foreach ($bikes as $bike) {
            $lechemin = $this->buildImagePath($bike);
            if ($filesystem->exists('./images/bikes/' . $lechemin)) {
                $imagePathsBike[] .= $this->buildImagePath($bike);
            }
        }

        return $this->render('listing/bikes.html.twig', [
            'bikes' => $bikes,
            'imagePathsBike' => $imagePathsBike,
        ]);
    }






    #[Route('/filtered/bikes', name: 'app_filtered_bikes')]
    public function filteredBikes(Request $request, EntityManagerInterface $entityManager): Response
    {
        $fuelType = $request->request->get('fuelType') === '1' ? null : $request->request->get('fuelType');
        $constructor = $request->request->get('constructor') === '1' ? null : $request->request->get('constructor');
        $model = $request->request->get('model') === '1' ? null : $request->request->get('model');
        $minCylindre = empty($request->request->get('minCylindre')) ? null : intval($request->request->get('minCylindre'));
        $maxCylindre = empty($request->request->get('maxCylindre')) ? null : intval($request->request->get('maxCylindre'));
        $minPrice = empty($request->request->get('minPrice')) ? null : intval($request->request->get('minPrice'));
        $maxPrice = empty($request->request->get('maxPrice')) ? null : intval($request->request->get('maxPrice'));
        $minKilometer = empty($request->request->get('minKilometer')) ? null : intval($request->request->get('minKilometer'));
        $maxKilometer = empty($request->request->get('maxKilometer')) ? null : intval($request->request->get('maxKilometer'));
        $minYear = empty($request->request->get('minYear')) ? null : intval($request->request->get('minYear'));
        $maxYear = empty($request->request->get('maxYear')) ? null : intval($request->request->get('maxYear'));


        $repository = $entityManager->getRepository(Vehicle::class);

        $carsQueryBuilder = $repository->createQueryBuilder('v')
            ->select('v', 'vf')
            ->leftJoin('v.vehicleFiltersId', 'vf')
            ->andWhere('vf.twoWheels = :twoWheels')
            ->setParameter('twoWheels', true);

        if ($fuelType && $fuelType !== null) {
            $carsQueryBuilder->andWhere('vf.fueltype = :fuelType')
                ->setParameter('fuelType', $fuelType);
        }

        if ($constructor && $constructor !== null) {
            $carsQueryBuilder->andWhere('vf.constructor = :constructor')
                ->setParameter('constructor', $constructor);
        }

        if ($model && $model !== null) {
            $carsQueryBuilder->andWhere('vf.model = :model')
                ->setParameter('model', $model);
        }

        if ($minCylindre && $minCylindre !== null) {
            $carsQueryBuilder->orWhere('vf.fiscalHorsePower >= :minCylindre')
                ->setParameter('minCylindre', $minCylindre);
        }

        if ($maxCylindre && $maxCylindre !== null) {
            $carsQueryBuilder->orWhere('vf.fiscalHorsePower >= :maxCylindre')
                ->setParameter('maxCylindre', $maxCylindre);
        }

        if ($minPrice && $minPrice !== null) {
            $carsQueryBuilder->orWhere('vf.buyPrice >= :minPrice')
                ->setParameter('minPrice', $minPrice);
        }

        if ($maxPrice && $maxPrice !== null) {
            $carsQueryBuilder->orWhere('vf.buyPrice <= :maxPrice')
                ->setParameter('maxPrice', $maxPrice);
        }

        if ($minKilometer && $minKilometer !== null) {
            $carsQueryBuilder->orWhere('v.kilometers >= :minKilometer')
                ->setParameter('minKilometer', $minKilometer);
        }

        if ($maxKilometer && $maxKilometer !== null) {
            $carsQueryBuilder->orWhere('v.kilometers <= :maxKilometer')
                ->setParameter('maxKilometer', $maxKilometer);
        }

        if ($minYear && $minYear !== null) {
            $carsQueryBuilder->orWhere('vf.year >= :minYear')
                ->setParameter('minYear', $minYear);
        }

        if ($maxYear && $maxYear !== null) {
            $carsQueryBuilder->orWhere('vf.year <= :maxYear')
                ->setParameter('maxYear', $maxYear);
        }

        $filteredCars = $carsQueryBuilder->getQuery()->getResult();

        $imagePathsBike = [];

        $filesystem = new Filesystem;

        foreach ($filteredCars as $bike) {
            $lechemin = $this->buildImagePath($bike);
            if ($filesystem->exists('./images/bikes/' . $lechemin)) {
                $imagePathsBike[] .= $this->buildImagePath($bike);
            }
        }

        return $this->render('listing/bikes.html.twig', [
            'bikes' => $filteredCars,
            'imagePathsBike' => $imagePathsBike,
        ]);
    }



    #[Route('/filtered/bikes', name: 'app_add_fav')]
    public function addFavCars(Request $request, EntityManagerInterface $entityManager): Response
    {
        $user = $this->getUser();

        if (!$user) {
            return $this->redirectToRoute('app_login');
        }

        $vehicleIdToAddToFavorites = $request->get('id');
        $isAlreadyFavorite = $entityManager->getRepository(Favorite::class)
            ->findOneBy([
                'userId' => $user,
                'vehicleId' => $vehicleIdToAddToFavorites,
            ]);

        if (!$isAlreadyFavorite) {
            $favorite = new Favorite();
            $favorite->setUserId($user);
            $vehicle = $entityManager->getRepository(Vehicle::class)->find($vehicleIdToAddToFavorites);
            $favorite->setVehicleId($vehicle);
            $favorite->setDateAdd(new \DateTime());
            $entityManager->persist($favorite);
            $entityManager->flush();
        } else {
            $entityManager->remove($isAlreadyFavorite);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_personnal_favories');
    }

    #[Route('/produit/search', name: 'search_results_product')]
    public function getSearchResultCars(Request $request, EntityManagerInterface $entityManager): Response
    {
        $query = $request->query->get('query');
        $repository = $entityManager->getRepository(Vehicle::class);

        $results = $repository->createQueryBuilder('v')
            ->select('v', 'vf')
            ->leftJoin('v.vehicleFiltersId', 'vf')
            ->andWhere('vf.constructor LIKE :query OR vf.model LIKE :query')
            ->setParameter('query', '%' . $query . '%')
            ->getQuery()
            ->getResult();

        return $this->render('listing/cars.html.twig', [
            'cars' => $results,
            'query' => $query,
        ]);
    }

    private function buildImagePath(Vehicle $car): string
    {
        $vehicleFilters = $car->getVehicleFiltersId();
        if ($vehicleFilters instanceof VehicleFilters && $vehicleFilters->isFourWheels() === true) {
            return 'voiture-' . $vehicleFilters->getConstructor() . '.png';
        } elseif ($vehicleFilters instanceof VehicleFilters && $vehicleFilters->isTwoWheels() === true) {
            return 'moto-' . $vehicleFilters->getConstructor() . '.png';
        }
        return 'default-image.png';
    }
}
