<?php

namespace App\Controller;

use App\Entity\Vehicle;
use App\Entity\VehicleFilters;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class HomePageController extends AbstractController
{
    #[Route('/', name: 'app_home_page')]
    public function index(Session $session, EntityManagerInterface $entityManager): Response
    {
        $repository = $entityManager->getRepository(Vehicle::class);

        $cars = $repository->createQueryBuilder('v')
            ->select('v', 'vf')
            ->leftJoin('v.vehicleFiltersId', 'vf')
            ->andWhere('vf.fourWheels = :fourWheels')
            ->setParameter('fourWheels', true)
            ->setMaxResults(4)
            ->getQuery()
            ->getResult();

        $bikes = $repository->createQueryBuilder('v')
            ->select('v', 'vf')
            ->leftJoin('v.vehicleFiltersId', 'vf')
            ->andWhere('vf.twoWheels = :twoWheels')
            ->setParameter('twoWheels', true)
            ->setMaxResults(4)
            ->getQuery()
            ->getResult();

        $imagePathsCar = [];
        $imagePathsBike = [];

        $filesystem = new Filesystem;

        foreach ($cars as $car) {
            $lechemin = $this->buildImagePath($car);
            if ($filesystem->exists('./images/cars/' . $lechemin)) {
                $imagePathsCar[] .= $this->buildImagePath($car);
            }
        }

        foreach ($bikes as $bike) {
            $lechemin = $this->buildImagePath($bike);
            if ($filesystem->exists('./images/bikes/' . $lechemin)) {
                $imagePathsBike[] .= $this->buildImagePath($bike);
            }
        }

        $repertoireImages = $this->getParameter('kernel.project_dir') . '/public/images/partenaires';

        $finder = new Finder();
        $images = [];

        foreach ($finder->files()->in($repertoireImages) as $fichier) {
            $images[] = $fichier->getRelativePathname();
        }

        $session->start();
        return $this->render('default/index.html.twig', [
            'controller_name' => 'HomePageController',
            'cars' => $cars,
            'bikes' => $bikes,
            'imagePathsCar' => $imagePathsCar,
            'imagePathsBike' => $imagePathsBike,
            'images' => $images,
        ]);
    }

    #[Route('/homePage/searchVehicle', name: 'search_results')]
    public function getSearchResult(Request $request, EntityManagerInterface $entityManager): Response
    {
        $query = $request->query->get('query');
        $repository = $entityManager->getRepository(Vehicle::class);

        $vehicles = $repository->createQueryBuilder('v')
            ->select('v', 'vf')
            ->leftJoin('v.vehicleFiltersId', 'vf')
            ->andWhere('vf.constructor LIKE :query OR vf.model LIKE :query')
            ->setParameter('query', '%' . $query . '%')
            ->getQuery()
            ->getResult();
        $filesystem = new Filesystem;
        $imagePaths = [];

        foreach ($vehicles as $vehicle) {
            $lechemin = $this->buildImagePath($vehicle);
            $vehicleFilters = $vehicle->getVehicleFiltersId();

            if ($vehicleFilters->isTwoWheels()) {
                $imagePaths[] =  $lechemin;
                return $this->render('listing/bikes.html.twig', [
                    'bikes' => $vehicles,
                    'query' => $query,
                    'imagePathsBike' => $imagePaths,
                ]);
            } elseif ($vehicleFilters->isFourWheels()) {
                $imagePaths[] =  $lechemin;
                return $this->render('listing/cars.html.twig', [
                    'cars' => $vehicles,
                    'query' => $query,
                    'imagePathsCar' => $imagePaths,
                ]);
            } else {
                $imagePaths[] = 'default-image.png';
            }
        }
    }

    private function buildImagePath(Vehicle $car): string
    {
        $vehicleFilters = $car->getVehicleFiltersId();
        if ($vehicleFilters instanceof VehicleFilters && $vehicleFilters->isFourWheels() === true) {
            return 'voiture-' . $vehicleFilters->getConstructor() . '.png';
        } elseif ($vehicleFilters instanceof VehicleFilters && $vehicleFilters->isTwoWheels() === true) {
            return 'moto-' . $vehicleFilters->getConstructor() . '.png';
        }
        return 'default-image.png';
    }
}
