# Projet Bonnie And Car

## Introduction au projet
Le but de ce projet est de reprendre le site : ```https://www.bonnieandcar.com``` pour le remettre au jour avec un style plus récent et épuré.
Pour ce faire, nous avons dut réalisé le projet au complet en équipe. Pour suivre l'avancement du projet, nous avons utilisé plusieurs outils :

## Outils utilisés
### Outils généraux
Dans un premier temps, nous aovns utilisé figma :
- figma pour les personnas et les user flow :
```https://www.figma.com/file/1AIR0IpHIjSbfMlyEEArd1/Whiteboard---Bonnie-and-Car?type=whiteboard&node-id=0-1&t=lqviEx5JEpprvwT3-0```
- figma pour les wireframes et maquettes :
```https://www.figma.com/file/lPbDHdcktbjaIR9b9lkrTM/Bonnie-and-Car?type=design&node-id=0-1&mode=design&t=eb62nVR7MhR3weK7-0```
- trello pour le suivi de la gestion de projet :
```https://trello.com/w/g1bonnieco```
- dbdiagram pour la mise en place de la base de façon plus schématique :
```https://dbdiagram.io/d/65c23a45ac844320ae910ea5```

### Outils techniques
De plus, sur ce projet, nous avons utilisé des fichiers tels qu'un gitignore ou bien un editorConfig.
- Le gitignore est utilisé surtout pour ne pas pousser les .env et .env.local, car on ne veux pas les fichiers de configurations de disponnible sur git, mais aussi pour éviter que le cache ni les logs de symfony viennent polluer nos merges request.
- l'editorConfig, quand à lui , nous a permis de nous mettre d'accord sur les normes de fins de lignes, les espaces entre chaques composants dans une balise html etc.. Mais aussi toutes les autres questions comme celle sur les indentations de twig. Grâce à lui, le code s'indente à la sauvegarde donc plus de perte de temps a devoir tout tabuler

## Environnement technique
Nous avons décider d'opté pour du symfony pour la relation back/front, car c'est un framework qui est simple d'utilisation mais complet avec en prime la touche française.
De plus, pour le style, on a opté pour du bootstrap qui est le meilleur gains de temps tout en gardant de beaux design en completant par des fichiers css fait de nos soins.

## Commande Symfony utiles pour la suite du projet :
- Démarer un server sans consulter les logs en continues
```symfony server:start -d```
- Créer son propre certificat ssl local :
```symfony.exe server:ca:install```
- Créer une entité (table dans la base de donnée)
```symfony console make:entity```
- Créer un controller (souvent associer à un twig)
```symfony console make:controller```
- a la suite de la création d'un controller on peux faire un formulaire tout prêt grâce a symfony
```symfony console make:form nom_du_formulaire```
- symfony console make:crud (ça sera pour la partie back-office surtout)
```symfony console make:crud```
- pour vider le cache de symfony (utile en prod)
```php bin/console cache:clear```

## Les chemins principaux a connaîtres :
Sur cette partie, je vais mettre les principals routes qui seront utiles :
- les différentes routes présentes dans le src :
    - src/Controller : Contient tous les controllers du projet
    - src/Entity : Contient toutes les entités
    - src/repository : gère la récupération et la persistance des entités
- la route templates : qui contiens tous les templates (en twig) du projet
- config/routes.yaml : permet de configurer ou de vérifier la configuration des différentes routes de notre projet
- config/packages/security.yaml : Permet de configurer toute la partie sécurité du projet (les différents rôles, parfeu, routes_admin etc...)
- public/css : regroupe les différents fichiers css disponnibles et utiles au bon fonctionnement des styles du projet.

## Fonctionnement de la base de donnée :
Tout d'abord, il faudra dupliquer le fichier .env et le renommer .env.local

### Comment créer sa base de donnée ?
Après avoir duppliquer le .env et fait un .env.local, il faudra remplir la ligne suivante :
``` DATABASE_URL="mysql://username:!password!@127.0.0.1:3306/dbName?charset=utf8mb4"```
puis, pour créer sa base de donnée en local, il faudra faire cette commande :
```symfony console doctrine:database:create```
#### attention avant de faire la suite, bien vérifier si toutes les entités ont été créer. Pour ça, se référer aux entités dans src/Entity
Ensuite, il faut mettre a jour le schémas de la base de donnée grâce a cette commande :
```symfony console doctrine:schema:update --force```

## les différentes tables du projet :
Le schémas complet de la base de donnée est fournis dans un dossier annexe (global_project_database)
### Table users {
- id (int)
- firstName (varchar)
- lastName (varchar)
- email (varchar)
- userName (varchar)
- Password (varchar)
- emailVerified (bool)
- profilePicture (varchar)
- address (varchar)
- postalCode (int)
- city (varchar)
### }

### Table vehicleFilters{
- id (int)
- constructor (varchar)
- model (varchar)
- buyPrice (int)
- locationPrice (int)
- twoWheels (boolean)
- fourWheels (boolean)
- trim (varchar)
- cnit (varchar)
- tvv (varchar)
- fuelType (varchar)
- hybrid (boolean)
- fiscalHorsePower (int)
- maxPower (int)
- cylinder (int)
- transmission (varchar)
- urbanConsumption (float)
- nonUrbanConsumption (float)
- averageConsumption (float)
- co2Emission (float)
- v9Critary (varchar)
- j1Critary (varchar)
### }

### Table vehicle{
- id (int)
- vehicleFiltersId (integer)
- productionYear (datetime)
- cirulationDate (datetime)
- technicalRevision (boolean)
- numberOwners (int)
- kilometers (int)
- color (varchar)
- doors (int)
- places (int)
- lenght (float)
### }

### Table vehiclePictures{
- id (int)
- vehicle_id (int)
- pictures (varchar)
### }

### Table favorite{
- id (int)
- usersId (int)
- vehicleId (int)
- addDate (datetime)
### }

### Table orders{
- id (int)
- usersId (int)
- vehicleId (int)
- ordersDate (datetime)
- cgpValidation (boolean)
### }

### Table productsForSale{
- id (int)
- usersId (int)
- vehicleId (int)
- title (varchar)
- description (varchar)
- publicatedDate (datetime)
- sale (boolean)
### }

### Ref: "vehicle"."vehicleFiltersId" < "vehicleFilters"."id"
### Ref: "vehiclePictures"."vehicle_id" < "vehicle"."id"
### Ref: "favorite"."usersId" < "users"."id"
### Ref: "favorite"."vehicleId" < "vehicle"."id"
### Ref: "users"."id" < "productsForSale"."usersId"
### Ref: "vehicle"."id" < "productsForSale"."vehicleId"
### Ref: "users"."id" < "orders"."usersId"
### Ref: "vehicle"."id" < "orders"."vehicleId"
