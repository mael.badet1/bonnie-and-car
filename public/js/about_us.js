document.addEventListener("DOMContentLoaded", function () { // Masquer initialement la FAQ des acheteurs
    document.getElementById("questionsAcheteur").style.display = "block"; // Afficher les questions des acheteurs au chargement
    document.getElementById("questionsVendeur").style.display = "none";
});

function afficherQuestions(type) { // Masquer toutes les div de questions
    var questionsAcheteur = document.getElementById('questionsAcheteur');
    var questionsVendeur = document.getElementById('questionsVendeur');
    questionsAcheteur.style.display = 'none';
    questionsVendeur.style.display = 'none';

    // Afficher la div de questions en fonction du type
    if (type === 'acheteur') {
        questionsAcheteur.style.display = 'block';
    } else if (type === 'vendeur') {
        questionsVendeur.style.display = 'block';
    }
}

function afficherReponse(id) {
    var sousParagraphe = document.getElementById(id);

    // Inverser l'affichage du sous-paragraphe
    sousParagraphe.style.display = (sousParagraphe.style.display === 'none') ? 'block' : 'none';
}
