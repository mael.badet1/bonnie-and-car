document.addEventListener('DOMContentLoaded', function () {
    let typeOptions = document.querySelectorAll('input[name="type"]');
    let cylindreeField = document.getElementById('cylindree');
    let chevauxField = document.getElementById('chevaux');

    typeOptions.forEach(function (option) {
        option.addEventListener('change', function () {
            if (option.value === 'moto' || option.value === 'scooter') {
                cylindreeField.style.display = 'block';
                chevauxField.style.display = 'none';
            } else if (option.value === 'voiture') {
                cylindreeField.style.display = 'none';
                chevauxField.style.display = 'block';
            } else {
                cylindreeField.style.display = 'none';
                chevauxField.style.display = 'none';
            }
        });
    });
});
